import { useState } from 'react';

import ProductList from './components/ProductList';
import Product from './components/Product';

import './App.css';

function App() {
  const [currentSale, setCurrentSale] = useState([])
  const [discountProduct, setDiscountProduct] = useState({});

  const handleClick = (ProductID) => {
    setCurrentSale([...currentSale, useProduct.find(product => product.id === ProductID)])
  }
   

  const discountGenerate = () =>{
    
    const randomizer = (min, max) =>{
      return Math.floor(Math.random() * (max - min) ) + min;            
    } 

    const ID = randomizer(1, 7)
    const percentage = randomizer(30, 90)

    const product = useProduct.filter(product => searchID(product, ID))[0]

    setDiscountProduct({id: product.id, name: product.name, price: product.price, discount: percentage})
    console.log(discountProduct)
  }

  const searchID = (product, ID) => {
    if(product.id === ID){
      return product
    }
  }

  const [useProduct] = useState([
    { id: 1, name: 'Smart TV LED 50', price: 1999.00 },
    { id: 2, name: 'PlayStation 5', price: 12000.00 },
    { id: 3, name: 'Notebook Acer Nitro 5', price: 5109.72},
    { id: 4, name: 'Headset s fio Logitech G935', price: 1359.00 },
    { id: 5, name: 'Tablet Samsung Galaxy Tab S7', price: 4844.05 },
    { id: 6, name: 'Cadeira Gamer Cruiser Preta FORTREK', price: 1215.16},
  ]);

  return (
    <div className="App">
      <header className="App-header">   
        <div className='shop'>
          <h1>Shop:</h1>
          <ProductList list={useProduct} handleClick={handleClick}/>
          <button onClick={discountGenerate}>Generate Discount</button>
          {discountProduct.discount && 
          <Product 
            product={discountProduct} 
            handleClick={() => setCurrentSale([...currentSale, discountProduct])}/>
          }
        </div>
          
        {currentSale.length>0 && 
          <div className='SaleCart'>
            <h1>CART:</h1>
            {currentSale.map(product => (
              product.discount ? 
              <div>{product.name}
                <span> R${product.price-(product.price/100*product.discount)}</span>
                <span style={{color:'red'}}> {product.discount}%</span>
              </div>
              :
              <div>{product.name}
                <span> R${product.price} </span>
              </div>
            ))}

            <h2>VALOR A SER PAGO:</h2>
            <div>R$ {currentSale.reduce((total, {price, discount}) => 
              discount ? (total+=(price-(price/100*discount))) : (total+=(price)),0)}
            </div>
          </div>
          }
      </header>
    </div>
  );
}

export default App;
