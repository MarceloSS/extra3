import Product from '../Product';

const ProductList = (props) => {
            
    return (
        <div>
            {props.list.map(product => 
                <Product product={product} handleClick={props.handleClick}/>
            )}
        </div>
    )
}

export default ProductList;