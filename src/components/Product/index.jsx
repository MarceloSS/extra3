const Product = (props) => {
    return(
        <li key={props.product.id} style={{margin:'5px'}}>
            <span>{props.product.name} </span>
            {props.product.discount ?
            <>
                <span style={{backgroundColor:'green'}}> R${props.product.price-(props.product.price/100*props.product.discount)}</span>
                <span style={{color:'red'}}> {props.product.discount}%</span>
            </>
            :
            <span style={{backgroundColor:'green'}}>R${props.product.price}</span>
            }
            <>  |  </>
            <button onClick={() => props.handleClick(props.product.id)}>add to cart</button>
        </li>
    )
}
export default Product;